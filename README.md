# battle-ship-test-task
![Screen shot](src/assets/screen-shot.png)

## Project setup
```
npm install
```

## Compile and hot-reload for development
```
npm run serve
```

##### Or compile and minify for production
```
npm run build
```
### Start WebSocket server
```
php -q server.php
```


