<?php
namespace app;

use app\Handlers\WebSocketHandler;

class WebSocketServer {

    private $connects;
    public function __construct() {
        session_start();
    }

    public function start() {
        $socket = stream_socket_server("tcp://0.0.0.0:8088", $errno, $errstr);

        if (!$socket) {
            die("$errstr ($errno)\n");
        }

        $connects = array();
        while (true) {
            //form an array of listening sockets
            $this->connects = $connects;
            $this->connects []= $socket;
            $write = $except = null;

            if (!stream_select($this->connects, $write, $except, null)) {//expect sockets available for reading (no timeout)
                break;
            }

            if (in_array($socket, $this->connects)) {//if there is a new connection
                //get new connection and Handshake
                if (($connect = stream_socket_accept($socket, -1)) && $info = $this->handshake($connect)) {
                    $connects[] = $connect;
                }
                unset($this->connects[ array_search($socket, $this->connects) ]);
            }


            foreach($this->connects as $connect) {
                $data = fread($connect, 10000000);
                $handler = new WebSocketHandler($connect);

                if (!$data) { //connection was closed
                    $handler->onClose();
                    unset($connects[ array_search($connect, $connects) ]);
                    continue;
                }

                $handler->onMessage($data);
            }
        }
    }

    private function handshake($connect) {
            $info = array();

            $line = fgets($connect);
            $header = explode(' ', $line);
            $info['method'] = $header[0];
            $info['uri'] = $header[1];

            //read headers from connection
            while ($line = rtrim(fgets($connect))) {
                if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                    $info[$matches[1]] = $matches[2];
                } else {
                    break;
                }
            }

            $address = explode(':', stream_socket_get_name($connect, true)); //get client address
            $info['ip'] = $address[0];
            $info['port'] = $address[1];

            if (empty($info['Sec-WebSocket-Key'])) {
                return false;
            }

            //send headers according to a web socket protocol
            $SecWebSocketAccept = base64_encode(pack('H*', sha1($info['Sec-WebSocket-Key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
            $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
                "Upgrade: websocket\r\n" .
                "Connection: Upgrade\r\n" .
                "Sec-WebSocket-Accept:$SecWebSocketAccept\r\n\r\n";
            fwrite($connect, $upgrade);

            return $info;

    }
}

