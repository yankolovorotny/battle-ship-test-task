<?php

namespace app\Models;


use app\Helpers\ShipBuilderHelper;
use app\Models\Ship\LShapedShipBuilder;
use app\Models\Ship\ShipDirector;
use app\Models\Ship\SimpleShipBuilder;

class Player
{
    public $shipsMap, $strikesMap;

    public function __construct($payload = null)
    {
        if (!empty($payload)) {
            $this->shipsMap = $payload['shipsMap'];
            $this->strikesMap = $payload['strikesMap'];
        } else {
            $this->shipsMap = $this->generateShipsMap();
            $this->strikesMap = $this->generateEmptyMap();
        }

    }

    /**
     * set up ships on the empty map
     * @return array
     */
    private function generateShipsMap(): array
    {
        $isValid = false;

        while (!$isValid) {
            $output = $this->generateEmptyMap();
            $ships = [];

            //init ship builders
            $lShapedShipBuilder = new LShapedShipBuilder();
            $simpleShipBuilder = new SimpleShipBuilder();

            //init ship director
            $shipDirector = new ShipDirector();

            //build L-shaped ship
            $shipDirector->setShipBuilder($lShapedShipBuilder);
            $shipDirector->buildShip($output);
            $output = $lShapedShipBuilder->getResult();
            $ships[] = "L";

            //build I-shaped ship
            $shipDirector->setShipBuilder($simpleShipBuilder);
            $shipDirector->buildShip($output, 4);
            $output = $simpleShipBuilder->getResult();
            $ships[] = 4;

            //build two dot-shaped ships
            for ($i = 0; $i < 2; $i++) {
                $shipDirector->setShipBuilder($simpleShipBuilder);
                $shipDirector->buildShip($output, 1);
                $output = $simpleShipBuilder->getResult();
                $ships[] = 1;
            }

            $isValid = ShipBuilderHelper::validate($output, $ships);
        }


        return $this->fixOutOfRange($output);
    }



    public function performTurn($x, $y)
    {
        $hit = false;
        $ships = [1, 4, 5];
        $coordinates = [];
        for ($i = 0; $i < 10; $i++) {
            for ($j = 0; $j < 10; $j++) {
                if (in_array($this->shipsMap[$x][$y], $ships)) {
                    $this->shipsMap[$x][$y] = -1;
                    $hit = true;
                }
            }
        }

        $end = true;
        foreach ($ships as $ship) {
            foreach ($this->shipsMap as $dimension) {
                if (in_array($ship, $dimension)) {
                    $end = false;
                }
            }
        }


        if (!$end && !$hit) {
            $expedientHit = false;
            //while random is hitted to -1 cell
            while (!$expedientHit) {
                $hX = rand(0, 9);
                $hY = rand(0, 9);
                if ($this->strikesMap[$hX][$hY] != -1) {
                    $this->strikesMap[$hX][$hY] = -1;
                    $coordinates = [
                        'x' => $hX,
                        'y' => $hY
                    ];
                    $expedientHit = true;
                }
            }
        }


        return [
            'end' => $end,
            'hit' => $hit,
            'coordinates' => $coordinates
        ];
    }

    public function makeTurn()
    {

        $expedientHit = false;
        $coordinates = [];
        //while random is hitted to -1 cell
        while (!$expedientHit) {
            $hX = rand(0, 9);
            $hY = rand(0, 9);
            if ($this->strikesMap[$hX][$hY] != -1) {
                $this->strikesMap[$hX][$hY] = -1;
                $coordinates = [
                    'x' => $hX,
                    'y' => $hY
                ];
                $expedientHit = true;
            }
        }
        return [
            'coordinates' => $coordinates
        ];
    }

    /**
     * Sometimes when creating a halo around ships the "-1" value goes beyond the 10x10 field
     * @param $output
     * @return array
     */
    private function fixOutOfRange($output): array
    {
        $result = [];
        for ($i = 0; $i < 10; $i++) {
            for ($j = 0; $j < 10; $j++) {
                $result[$i][$j] = $output[$i][$j];
            }
        }
        return $result;
    }

    /**
     * Generate 10x10 empty field
     * @return array
     */
    private function generateEmptyMap(): array
    {
        $output = [];
        for ($i = 0; $i < 10; $i++) {
            for ($j = 0; $j < 10; $j++) {
                $output[$i][$j] = 0;
            }
        }
        return $output;
    }

    public function getSessionId()
    {
        return session_id();
    }

}