<?php

namespace app\Models;


use app\Helpers\WebSocketHelper;

class Message
{
    public static function send($connect, $payload, $command = 'callback')
    {
        $output = [
            "command" => $command,
            "payload" => $payload,
        ];
        fwrite($connect, WebSocketHelper::encode(json_encode($output)));
    }
}