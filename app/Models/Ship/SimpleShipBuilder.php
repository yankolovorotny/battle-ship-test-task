<?php

namespace app\Models\Ship;


use app\Helpers\ShipBuilderHelper;

class SimpleShipBuilder implements IShipBuilder
{
    private $result;

    public function buildShip($output, $decksShip)
    {
        $isShipFlag = false;

        while (!$isShipFlag) {
            list($x, $y, $dx, $dy) = ShipBuilderHelper::generateXYDirection();


            for ($i = 0; $i < $decksShip; $i++) { // location depending on dx and dy

                $isShipFlag = true;
                $xyPos = $output[$y + $i * $dy][$x + $i * $dx];

                if ($dx == 1 && $dy == 0 && $xyPos != -1 && $xyPos < 1 && ($x + $i) < 10) continue; // right
                else if ($dx == 0 && $dy == 1 && $xyPos != -1 && $xyPos < 1 && ($y + $i) < 10) continue; // down
                else if ($dx == -1 && $dy == 0 && $xyPos != -1 && $xyPos < 1 && ($x - $i) > 0) continue; // left
                else if ($dx == 0 && $dy == -1 && $xyPos != -1 && $xyPos < 1 && ($y - $i) > 0) continue; // up
                else $isShipFlag = false;
            }

            if ($isShipFlag == true) { // place the L-shaped ship on the playing field
                for ($i = 0; $i < $decksShip; $i++) {
                    $output[$y + $i * $dy][$x + $i * $dx] = $decksShip;
                }
                $output = ShipBuilderHelper::traceSimpleShip($output, $x, $y, $dx, $dy, $decksShip);
            }
        }

        $this->result = $output;
    }

    public function getResult()
    {
        $result = $this->result;
        $this->reset();
        return $result;
    }

    function reset()
    {
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }
}