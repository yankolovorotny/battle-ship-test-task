<?php

namespace app\Models\Ship;


class ShipDirector
{
    private $shipBuilder;

    public function setShipBuilder(IShipBuilder $shipBuilder)
    {
        $this->shipBuilder = $shipBuilder;
    }

    public function buildShip($output, $decksShip = null)
    {
        $this->shipBuilder->buildShip($output, $decksShip);
    }
}