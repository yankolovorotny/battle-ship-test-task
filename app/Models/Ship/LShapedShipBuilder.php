<?php

namespace app\Models\Ship;


use app\Helpers\ShipBuilderHelper;

class LShapedShipBuilder implements IShipBuilder
{
    private $result;

    public function buildShip($output, $decksShip = null)
    {
        $isShipFlag = false;

        while (!$isShipFlag) {
            list($x, $y, $dx, $dy) = ShipBuilderHelper::generateXYDirection();


            for ($i = 0; $i < 3; $i++) { // location depending on dx and dy

                $isShipFlag = true;
                $xyPos = $output[$y + $i * $dy][$x + $i * $dx];

                //check if appendix of L is not out of range
                if ($dx == 1 && $dy == 0 && ($y + 2 * $dy - 1) >= 0 && ($y + 2 * $dy - 1) < 10 && ($x + 2 * $dx) >= 0 && ($x + 2 * $dx) < 10) continue; // right
                else if ($dx == 0 && $dy == 1 && ($y + 2 * $dy) >= 0 && ($y + 2 * $dy) < 10 && ($x + 2 * $dx + 1) >= 0 && ($x + 2 * $dx + 1) < 10) continue; // down
                else if ($dx == -1 && $dy == 0 && ($y + 2 * $dy + 1) >= 0 && ($y + 2 * $dy + 1) < 10 && ($x + 2 * $dx) >= 0 && ($x + 2 * $dx) < 10) continue; // left
                else if ($dx == 0 && $dy == -1 && ($y + 2 * $dy) >= 0 && ($y + 2 * $dy) < 10 && ($x + 2 * $dx - 1) >= 0 && ($x + 2 * $dx - 1) < 10) continue; // up
                else $isShipFlag = false;

                //check if ship doesn't touch any another ship
                if ($dx == 1 && $dy == 0 && $xyPos != -1 && $output[$y + 2 * $dy - 1][$x + 2 * $dx] != -1 && $xyPos < 1 && ($x + $i) < 10) continue; // right
                else if ($dx == 0 && $dy == 1 && $xyPos != -1 && $output[$y + 2 * $dy][$x + 2 * $dx + 1] != -1 && $xyPos < 1 && ($y + $i) < 10) continue; // down
                else if ($dx == -1 && $dy == 0 && $xyPos != -1 && $output[$y + 2 * $dy + 1][$x + 2 * $dx] != -1 && $xyPos < 1 && ($x - $i) > 0) continue; // left
                else if ($dx == 0 && $dy == -1 && $xyPos != -1 && $output[$y + 2 * $dy][$x + 2 * $dx - 1] != -1 && $xyPos < 1 && ($y - $i) > 0) continue; // up
                else $isShipFlag = false;
            }

            // place the L-shaped ship on the playing field
            if ($isShipFlag == true) {

                $output = ShipBuilderHelper::traceSimpleShip($output, $x, $y, $dx, $dy, 3);

                for ($i = 0; $i < 3; $i++) {
                    $output[$y + $i * $dy][$x + $i * $dx] = 5; // 5 is L-shaped ship

                }

                if ($dx == 1 && $dy == 0) {
                    $output[$y + 2 * $dy - 1][$x + 2 * $dx] = 5; // right
                    $output[$y + 2 * $dy - 1][$x + 2 * $dx - 1] = -1;
                    $output[$y + 2 * $dy - 1][$x + 2 * $dx + 1] = -1;
                    for ($j = 1; $j <= 3; $j++) {
                        $output[$y + 2 * $dy - 2][$x + 2 * $dx - 2 + $j] = -1;
                    }
                } else if ($dx == 0 && $dy == 1) {
                    $output[$y + 2 * $dy][$x + 2 * $dx + 1] = 5; // down
                    $output[$y + 2 * $dy - 1][$x + 2 * $dx + 1] = -1;
                    $output[$y + 2 * $dy + 1][$x + 2 * $dx + 1] = -1;
                    for ($j = 1; $j <= 3; $j++) {
                        $output[$y + 2 * $dy - 2 + $j][$x + 2 * $dx + 2] = -1;
                    }
                } else if ($dx == -1 && $dy == 0) {
                    $output[$y + 2 * $dy + 1][$x + 2 * $dx] = 5; // left

                    $output[$y + 2 * $dy + 1][$x + 2 * $dx - 1] = -1;
                    $output[$y + 2 * $dy + 1][$x + 2 * $dx + 1] = -1;
                    for ($j = 1; $j <= 3; $j++) {
                        $output[$y + 2 * $dy + 2][$x + 2 * $dx - 2 + $j] = -1;
                    }
                } else {
                    $output[$y + 2 * $dy][$x + 2 * $dx - 1] = 5; // up
                    $output[$y + 2 * $dy - 1][$x + 2 * $dx - 1] = -1;
                    $output[$y + 2 * $dy + 1][$x + 2 * $dx - 1] = -1;
                    for ($j = 1; $j <= 3; $j++) {
                        $output[$y + 2 * $dy - 2 + $j][$x + 2 * $dx - 2] = -1;
                    }
                }
            }
        }

        $this->result = $output;
    }

    public function getResult()
    {
        $result = $this->result;
        $this->reset();
        return $result;
    }

    function reset()
    {
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }
}