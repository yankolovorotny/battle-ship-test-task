<?php

namespace app\Models\Ship;


interface IShipBuilder
{
    public function buildShip($field, $decksShip);
}