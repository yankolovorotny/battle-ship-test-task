<?php

namespace app\Models\GameSource;


use app\Extensions\SimpleStorage;

class SimpleStorageGameSource implements IGameSource
{

    public function load($authorizeID)
    {
        $storage = new SimpleStorage();
        $stored = $storage->get($authorizeID);

        return $stored;
    }

    public function save($authorizeID, $payload)
    {
        $storage = new SimpleStorage();
        $storage->put($authorizeID, $payload);
    }
}