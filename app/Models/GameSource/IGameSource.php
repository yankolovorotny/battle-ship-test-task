<?php

namespace app\Models\GameSource;


interface IGameSource
{
    public function load($authorizeID);
    public function save($authorizeID, $payload);
}