<?php

namespace app\Models;


use app\Models\GameSource\IGameSource;
use app\Models\GameSource\SimpleStorageGameSource;

class Game
{
    protected $connection;
    private $player;
    private $gameSource;

    public function __construct($connection, IGameSource $source)
    {
        $this->connection = $connection;

        //here we can use any type of data storage, according to Open-Closed Principle
        $this->gameSource = $source;
    }

    /**
     * prepares computer player for game;
     * returns generated ships map to client.
     * NOTE: generated data for user is not stored on back-end,
     * it's just allows to save code on the front-end
     * @param $payload
     */
    public function prepare($payload)
    {
        //prepare computer
        $this->player = new Player();

        //prepare user
        $output = new Player();

//        for ($i = 0; $i < 10; $i++) {
//            for ($j = 0; $j < 10; $j++) {
//                echo $output->shipsMap[$i][$j] . ' ';
//            }
//            echo "\n";
//        }
//        echo "\n";

        $this->gameSource->save($this->player->getSessionId(), $this->player);

        Message::send($this->connection, ["map" => $output->shipsMap, "authorizeID" => $output->getSessionId()], 'authorizeCallback');
    }

    /**
     * performs user turn
     * @param $payload
     */
    public function playerTurn($payload)
    {
        $data = $this->gameSource->load($payload->authorizeID);
        $this->player = new Player($data);
        $output = $this->player->performTurn($payload->x, $payload->y);
        $this->gameSource->save($this->player->getSessionId(), $this->player);
        Message::send($this->connection, $output, 'playerTurnCallback');
    }

    /**
     * performs computer turn
     * @param $payload
     */
    public function computerTurn($payload)
    {
        $data = $this->gameSource->load($payload->authorizeID);
        $this->player = new Player($data);
        $output = $this->player->makeTurn();
        $this->gameSource->save($this->player->getSessionId(), $this->player);
        Message::send($this->connection, $output, 'computerTurnCallback');
    }

}