<?php

namespace app\Handlers;


use app\Helpers\WebSocketHelper;
use app\Models\Game;
use app\Models\GameSource\SimpleStorageGameSource;

class WebSocketHandler {

    private $connection;
    private $game;

    /**
     * WebSocketHandler constructor.
     * @param $connection
     */
    public function __construct($connection) {
        $this->connection = $connection;
        $this->game = new Game($this->connection, new SimpleStorageGameSource());
    }

    function onClose() {
        fclose($this->connection);
    }

    function onMessage($data) {
        $input = WebSocketHelper::decode($data);
        $payload = json_decode($input['payload']);
        if(!empty($payload->command)) {
            call_user_func_array(array($this->game,$payload->command), array($payload));
        }
    }
}