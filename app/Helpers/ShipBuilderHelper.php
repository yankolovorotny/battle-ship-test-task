<?php

namespace app\Helpers;


class ShipBuilderHelper
{
    public static function traceSimpleShip($output, $x, $y, $dx, $dy, $decksShip): array
    {
        $height = 10;

        //filling the fields around the ship -1 horizontally to the left
        if ($dx == -1 && $dy == 0) {
            $dx = 1;
            $dy = 0;
            $x -= $decksShip - 1;
            $w = $decksShip + 2;
            $h = 3;

            $dtX = 0; // X offset
            $dtY = 0; // Y offset

            if ($x == 0 && $y == 0) {
                $dtX = 0;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0 && $y == $height - 1) {
                $dtX = 0;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0) {
                $dtX = 0;
                $dtY = 1;
                $w--;
            } else if ($x != 0 && $y == 0 && $x != $height - $decksShip) {
                $dtX = 1;
                $dtY = 0;
                $h--;
            } else if ($x == $height - $decksShip && $y != 0 && $y == $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == $height - $decksShip && $y != 0 && $y != $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $w--;
            } else if ($x == $height - $decksShip && $y == 0) {
                $dtX = 1;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x != 0 && $y != 0 && $y == $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $h--;
            } else if ($x != 0 && $y != 0) {
                $dtX = 1;
                $dtY = 1;
            }

            for ($i = 0; $i < $h; $i++) {
                for ($j = 0; $j < $w; $j++) {
                    if ($output[($y + $i - $dtY)][($x + $j - $dtX) * $dx] == $decksShip) continue;
                    $output[($y + $i - $dtY)][($x + $j - $dtX) * $dx] = -1;
                }
            }
        }

        //filling the fields around the ship -1 horizontally to the right
        if ($dx == 1 && $dy == 0) {
            $w = $decksShip + 2;
            $h = 3;
            $widthDeck = $w;
            $dtX = 0;
            $dtY = 0;

            if ($x == 0 && $y == 0) {
                $dtX = 0;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0 && $y == $height - 1) {
                $dtX = 0;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0) {
                $dtX = 0;
                $dtY = 1;
                $w--;
            } else if ($x != 0 && $y == 0 && $x != $height - $decksShip) {
                $dtX = 1;
                $dtY = 0;
                $h--;
            } else if ($x == $height - $decksShip && $y != 0 && $y == $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == $height - $decksShip && $y != 0 && $y != $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $w--;
            } else if ($x == $height - $decksShip && $y == 0) {
                $dtX = 1;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x != 0 && $y != 0 && $y == $height - 1) {
                $dtX = 1;
                $dtY = 1;
                $h--;
            } else if ($x != 0 && $y != 0) {
                $dtX = 1;
                $dtY = 1;
            }


            for ($i = 0; $i < $h; $i++) {
                for ($j = 0; $j < $w; $j++) {
                    if ($output[($y + $i - $dtY)][($x + $j - $dtX) * $dx] == $decksShip) continue;
                    $output[($y + $i - $dtY)][($x + $j - $dtX) * $dx] = -1;
                }
            }
        }

        //filling the fields around the ship -1 vertically up from the beginning of the coordinate
        if ($dx == 0 && $dy == -1) {
            $y -= $decksShip - 1;
            $dx = 0;
            $dy = 1;

            $w = 3;
            $h = $decksShip + 2;
            $widthDeck = $w;
            $dtX = 0;
            $dtY = 0;

            if ($x == 0 && $y == 0) {
                $dtX = 0;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 0;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0) {
                $dtX = 0;
                $dtY = 1;
                $w--;
            } else if ($x != 0 && $y == 0 && $x != $height - 1) {
                $dtX = 1;
                $dtY = 0;
                $h--;
            } else if ($x == $height - 1 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == $height - 1 && $y != 0 && $y != $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $w--;
            } else if ($x == $height - 1 && $y == 0) {
                $dtX = 1;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x != 0 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $h--;
            } else if ($x != 0 && $y != 0) {
                $dtX = 1;
                $dtY = 1;
            }


            for ($i = 0; $i < $h; $i++) {
                for ($j = 0; $j < $w; $j++) {
                    if ($output[($y + $i - $dtY) * $dy][($x + $j - $dtX)] == $decksShip) continue;
                    $output[($y + $i - $dtY) * $dy][($x + $j - $dtX)] = -1;
                }
            }
        }

        //filling the fields around the ship -1 vertically down from the beginning of the coordinate
        if ($dx == 0 && $dy == 1) {
            $w = 3;
            $h = $decksShip + 2;
            $widthDeck = $w;
            $dtX = 0;
            $dtY = 0;

            if ($x == 0 && $y == 0) {
                $dtX = 0;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 0;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == 0 && $y != 0) {
                $dtX = 0;
                $dtY = 1;
                $w--;
            } else if ($x != 0 && $y == 0 && $x != $height - 1) {
                $dtX = 1;
                $dtY = 0;
                $h--;
            } else if ($x == $height - 1 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $h--;
                $w--;
            } else if ($x == $height - 1 && $y != 0 && $y != $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $w--;
            } else if ($x == $height - 1 && $y == 0) {
                $dtX = 1;
                $dtY = 0;
                $h--;
                $w--;
            } else if ($x != 0 && $y != 0 && $y == $height - $decksShip) {
                $dtX = 1;
                $dtY = 1;
                $h--;
            } else if ($x != 0 && $y != 0) {
                $dtX = 1;
                $dtY = 1;
            }


            for ($i = 0; $i < $h; $i++) {
                for ($j = 0; $j < $w; $j++) {
                    if ($output[($y + $i - $dtY) * $dy][($x + $j - $dtX)] == $decksShip) continue;
                    $output[($y + $i - $dtY) * $dy][($x + $j - $dtX)] = -1;
                }
            }
        }

        return $output;
    }

    public static function generateXYDirection(): array
    {
        $rotation = rand(1, 4);
        $dx = 0;
        $dy = 0;

        switch ($rotation) {
            case 1:
                $dx = 1;
                $dy = 0; // right
                break;
            case 2:
                $dx = -1;
                $dy = 0; // left
                break;
            case 3:
                $dx = 0;
                $dy = -1; // up
                break;
            case 4:
                $dx = 0;
                $dy = 1; // down
                break;
        }

        return [$x = rand() % 10, $y = rand() % 10, $dx, $dy];
    }

    public static function validate($output, $ships)
    {
        $isValid = true;

        $oneDimensionArray = [];
        foreach ($output as $row) {
            $oneDimensionArray = array_merge($oneDimensionArray, $row);
        }

        foreach (array_count_values($ships) as $decksShip => $amount) {

            switch ($decksShip) {
                case "L" :
                    if (array_count_values($oneDimensionArray)[5] !== 4 * $amount)
                        $isValid = false;
                    break;
                default :
                    if (array_count_values($oneDimensionArray)[$decksShip] !== $decksShip * $amount)
                        $isValid = false;
                    break;
            }

        }
        return $isValid;
    }
}