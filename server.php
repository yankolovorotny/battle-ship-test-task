<?php

use app\WebSocketServer;

ini_set('error_reporting', 0);

function __autoload($class_name) {
    $class_name = str_replace("\\", "/", $class_name);
    include_once __DIR__.'/'.$class_name.".php";
}

$server = new WebSocketServer();
$server->start();
